import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  urlAPI: 'http://intranet.endicon.com.br:8000/',
  authenticated: false,
  user: null,
  userInfo: {
    messages: [{1: 'test', 2: 'test'}],
    notifications: [],
    tasks: []
  },
  erroAcesso: false,
  regionais: [],
  polos: [],
  localidadespolo: [],
  funcoes: [],
  funcionarios: [],
  treinamentos: [],
  treinamentosRealizados: [],
  processos: [],
  tiposprocesso: [],
  equipamentoscoletivos: [],
  equipamentosindividuais: [],
  perguntasrisco: [],
  riscosidentificados: [],
  analisesrisco: [],
  desvios: [],
  comunicadosInternos: [],
  usuarios: [],
  tarefas: [],
  perguntasrelatosinternos: [],
  relatosInternos: []
}

const mutations = {
  SET_USER (state, user) {
    state.user = user
  },
  ALTERAR_AUTENTIFICACAO (state, payload) {
    state.authenticated = payload.auth
  },
  ALTERAR_ERROACESSO (state, payload) {
    state.erroAcesso = payload.erro
  },
  ATUALIZAR_REGIONAIS (state, payload) {
    state.regionais = payload
  },
  ATUALIZAR_REGIONAL (state, payload) {
    state.regionais[payload.index].nome = payload.nome
    state.regionais[payload.index].estado = payload.estado
  },
  ADD_REGIONAL (state, payload) {
    state.regionais.push(payload)
  },
  ATUALIZAR_POLOS (state, payload) {
    state.polos = payload
  },
  ATUALIZAR_POLO (state, payload) {
    state.polos[payload.index].nome = payload.nome
    state.polos[payload.index].regional = payload.regional
  },
  ADD_POLO (state, payload) {
    state.polos.push(payload)
  },
  ATUALIZAR_LOCALIDADESPOLO (state, payload) {
    state.localidadespolo = payload
  },
  ATUALIZAR_LOCALIDADEPOLO (state, payload) {
    state.localidadespolo[payload.index].nome = payload.nome
    state.localidadespolo[payload.index].polo = payload.polo
  },
  ADD_LOCALIDADEPOLO (state, payload) {
    state.localidadespolo.push(payload)
  },
  ATUALIZAR_FUNCOES (state, payload) {
    state.funcoes = payload
  },
  ATUALIZAR_FUNCAO (state, payload) {
    state.funcoes[payload.index].nome = payload.nome
  },
  ADD_FUNCAO (state, payload) {
    state.funcoes.push(payload)
  },
  ATUALIZAR_FUNCIONARIOS (state, payload) {
    state.funcionarios = payload
  },
  ATUALIZAR_FUNCIONARIO (state, payload) {
    state.funcionarios[payload.index].nome = payload.nome
    state.funcionarios[payload.index].dataNascimento = payload.dataNascimento
    state.funcionarios[payload.index].sexo = payload.sexo
    state.funcionarios[payload.index].estadoCivil = payload.estadoCivil
    state.funcionarios[payload.index].inicioFuncao = payload.inicioFuncao
    state.funcionarios[payload.index].grauInstrucao = payload.grauInstrucao
    state.funcionarios[payload.index].estadoInstrucao = payload.estadoInstrucao
    state.funcionarios[payload.index].inicioRefeicao = payload.inicioRefeicao
    state.funcionarios[payload.index].fimRefeicao = payload.fimRefeicao
    state.funcionarios[payload.index].turnoTrabalho = payload.turnoTrabalho
    state.funcionarios[payload.index].nacionalidade = payload.nacionalidade
    state.funcionarios[payload.index].funcao = payload.funcao
    state.funcionarios[payload.index].local = payload.local
    state.funcionarios[payload.index].localidadePolo = payload.localidadePolo
    state.funcionarios[payload.index].horaExtra = payload.horaExtra
  },
  ADD_FUNCIONARIO (state, payload) {
    state.funcionarios.push(payload)
  },
  ATUALIZAR_TREINAMENTOS (state, payload) {
    state.treinamentos = payload
  },
  ATUALIZAR_TREINAMENTO (state, payload) {
    state.treinamentos[payload.index].nome = payload.nome
    state.treinamentos[payload.index].codigo = payload.codigo
    state.treinamentos[payload.index].conteudoProgramatico = payload.conteudoProgramatico
  },
  ADD_TREINAMENTO (state, payload) {
    state.treinamentos.push(payload)
  },
  ATUALIZAR_TREINAMENTOSREALIZADOS (state, payload) {
    state.treinamentosRealizados = payload
  },
  ATUALIZAR_TREINAMENTOREALIZADO (state, payload) {
    state.treinamentosRealizados[payload.index].data = payload.data
    state.treinamentosRealizados[payload.index].instrutor = payload.instrutor
    state.treinamentosRealizados[payload.index].cargaHoraria = payload.cargaHoraria
  },
  ADD_TREINAMENTOREALIZADO (state, payload) {
    state.treinamentosRealizados.push(payload)
  },
  ATUALIZAR_PROCESSOS (state, payload) {
    state.processos = payload
  },
  ATUALIZAR_PROCESSO (state, payload) {
    state.processos[payload.index].nome = payload.nome
    state.processos[payload.index].estado = payload.estado
  },
  ADD_PROCESSO (state, payload) {
    state.processos.push(payload)
  },
  ATUALIZAR_TIPOSPROCESSO (state, payload) {
    state.tiposprocesso = payload
  },
  ATUALIZAR_TIPOPROCESSO (state, payload) {
    state.tiposprocesso[payload.index].nome = payload.nome
    state.tiposprocesso[payload.index].processo = payload.processo
    state.tiposprocesso[payload.index].regional = payload.regional
  },
  ADD_TIPOPROCESSO (state, payload) {
    state.tiposprocesso.push(payload)
  },
  ATUALIZAR_EQUIPAMENTOSINDIVIDUAIS (state, payload) {
    state.equipamentosindividuais = payload
  },
  ATUALIZAR_EQUIPAMENTOINDIVIDUAL (state, payload) {
    state.equipamentosindividuais[payload.index].nome = payload.nome
  },
  ADD_EQUIPAMENTOINDIVIDUAL (state, payload) {
    state.equipamentosindividuais.push(payload)
  },
  ATUALIZAR_EQUIPAMENTOSCOLETIVOS (state, payload) {
    state.equipamentoscoletivos = payload
  },
  ATUALIZAR_EQUIPAMENTOCOLETIVO (state, payload) {
    state.equipamentoscoletivos[payload.index].nome = payload.nome
  },
  ADD_EQUIPAMENTOCOLETIVO (state, payload) {
    state.equipamentoscoletivos.push(payload)
  },
  ATUALIZAR_PERGUNTASRISCO (state, payload) {
    state.perguntasrisco = payload
  },
  ATUALIZAR_PERGUNTARISCO (state, payload) {
    state.perguntasrisco[payload.index].pergunta = payload.pergunta
  },
  ADD_PERGUNTARISCO (state, payload) {
    state.perguntasrisco.push(payload)
  },
  ATUALIZAR_RISCOSIDENTIFICADOS (state, payload) {
    state.riscosidentificados = payload
  },
  ATUALIZAR_RISCOIDENTIFICADO (state, payload) {
    state.riscosidentificados[payload.index].nome = payload.nome
  },
  ADD_RISCOIDENTIFICADO (state, payload) {
    state.riscosidentificados.push(payload)
  },
  ATUALIZAR_ANALISESRISCO (state, payload) {
    state.analisesrisco = payload
  },
  ATUALIZAR_ANALISERISCO (state, payload) {
    state.analisesrisco[payload.index].dataCriacao = payload.dataCriacao
    state.analisesrisco[payload.index].horaCriacao = payload.horaCriacao
    state.analisesrisco[payload.index].dataOcorrido = payload.dataOcorrido
    state.analisesrisco[payload.index].descricaoAtividade = payload.descricaoAtividade
    state.analisesrisco[payload.index].atividadeSegura = payload.atividadeSegura
    state.analisesrisco[payload.index].justificativa = payload.justificativa
    state.analisesrisco[payload.index].observacao = payload.observacao
    state.analisesrisco[payload.index].local = payload.local
    state.analisesrisco[payload.index].funcionario = payload.funcionario
    state.analisesrisco[payload.index].tipoProcesso = payload.tipoProcesso
    state.analisesrisco[payload.index].localidadePolo = payload.localidadePolo
    state.analisesrisco[payload.index].equipamentosColetivos = payload.equipamentosColetivos
    state.analisesrisco[payload.index].equipamentosIndividuais = payload.equipamentosIndividuais
    state.analisesrisco[payload.index].riscosIdentificados = payload.riscosIdentificados
    state.analisesrisco[payload.index].perguntasRespondidas = payload.perguntasRespondidas
    state.analisesrisco[payload.index].membros = payload.membros
  },
  ADD_ANALISERISCO (state, payload) {
    state.analisesrisco.push(payload)
  },
  ADD_EQUIPAMENTOSCOLETIVOSANALISERISCO (state, payload) {
    state.analisesrisco[payload.index].equipamentosColetivos.push(payload.equipamentoColetivo)
  },
  ADD_EQUIPAMENTOSINDIVIDUAISANALISERISCO (state, payload) {
    state.analisesrisco[payload.index].equipamentosIndividuais.push(payload.equipamentoIndividual)
  },
  ADD_MEMBROSANALISERISCO (state, payload) {
    state.analisesrisco[payload.index].membros.push(payload.membro)
  },
  ADD_RISCOSIDENTIFICADOSANALISERISCO (state, payload) {
    state.analisesrisco[payload.index].riscosIdentificados.push(payload.riscoidentificado)
  },
  ADD_RESPOSTASRISCO (state, payload) {
    state.analisesrisco[payload.index].perguntasRespondidas.push(payload.perguntaRespondida)
  },
  ADD_DESVIO (state, payload) {
    state.desvios.push(payload)
  },
  ATUALIZAR_DESVIOS (state, payload) {
    state.desvios = payload
  },
  ATUALIZAR_DESVIO (state, payload) {
    state.desvios[payload.index].dataOcorrido = payload.dataOcorrido
    state.desvios[payload.index].horaOcorrido = payload.horaOcorrido
    state.desvios[payload.index].registro = payload.registro
    state.desvios[payload.index].local = payload.local
    state.desvios[payload.index].dataCriacao = payload.dataCriacao
    state.desvios[payload.index].horaCriacao = payload.horaCriacao
    state.desvios[payload.index].tipo = payload.tipo
    state.desvios[payload.index].funcionario = payload.funcionario
    state.desvios[payload.index].localidadePolo = payload.localidadePolo
  },
  ADD_COMUNICADOINTERNO (state, payload) {
    state.comunicadosInternos.push(payload)
  },
  ATUALIZAR_COMUNICADOSINTERNOS (state, payload) {
    state.comunicadosInternos = payload
  },
  ATUALIZAR_COMUNICADOINTERNO (state, payload) {
    state.comunicadosInternos[payload.index].horaCriacao = payload.horaCriacao
    state.comunicadosInternos[payload.index].dataCriacao = payload.dataCriacao
    state.comunicadosInternos[payload.index].tema = payload.tema
    state.comunicadosInternos[payload.index].descricao = payload.descricao
    state.comunicadosInternos[payload.index].dataOcorrido = payload.dataOcorrido
    state.comunicadosInternos[payload.index].horaOcorrido = payload.horaOcorrido
    state.comunicadosInternos[payload.index].danosPessoais = payload.danosPessoais
    state.comunicadosInternos[payload.index].danosMateriaisAmbientais = payload.danosMateriaisAmbientais
    state.comunicadosInternos[payload.index].local = payload.local
    state.comunicadosInternos[payload.index].processo = payload.processo
    state.comunicadosInternos[payload.index].tipo = payload.tipo
    state.comunicadosInternos[payload.index].analiseRisco = payload.analiseRisco
    state.comunicadosInternos[payload.index].funcionario = payload.funcionario
    state.comunicadosInternos[payload.index].localidadePolo = payload.localidadePolo
    state.comunicadosInternos[payload.index].tarefa = payload.tarefa
    state.comunicadosInternos[payload.index].codigo = payload.codigo
    state.comunicadosInternos[payload.index].representanteSesmt = payload.representanteSesmt
    state.comunicadosInternos[payload.index].gravidade = payload.gravidade
    state.comunicadosInternos[payload.index].naturezaLesao = payload.naturezaLesao
    state.comunicadosInternos[payload.index].parteCorpo = payload.parteCorpo
    state.comunicadosInternos[payload.index].especificacaoRisco = payload.especificacaoRisco
    state.comunicadosInternos[payload.index].numeroCat = payload.numeroCat
  },
  ADD_RESIDUO (state, payload) {
    state.comunicadosInternos[payload.index].residuos.push(payload.residuo)
  },
  ADD_ENVOLVIDO (state, payload) {
    state.comunicadosInternos[payload.index].envolvidos.push(payload.envolvido)
  },
  ADD_ENVOLVIDOFUNCIONARIO (state, payload) {
    state.comunicadosInternos[payload.index].envolvidosFuncionarios.push(payload.envolvido)
  },
  ATUALIZAR_USUARIOS (state, payload) {
    state.usuarios = payload
  },
  ATUALIZAR_USUARIO (state, payload) {
    state.usuarios[payload.index].login = payload.login
    state.usuarios[payload.index].permissao = payload.permissao
    state.usuarios[payload.index].ultimoAcesso = payload.ultimoAcesso
  },
  ADD_USUARIO (state, payload) {
    state.usuarios.push(payload)
  },
  ATUALIZAR_TAREFAS (state, payload) {
    state.tarefas = payload
  },
  ATUALIZAR_TAREFA (state, payload) {
    state.tarefas[payload.index].nome = payload.nome
    state.tarefas[payload.index].processo = payload.processo
  },
  ADD_TAREFA (state, payload) {
    state.tarefas.push(payload)
  },
  ATUALIZAR_PERGUNTASRELATOSINTERNOS (state, payload) {
    state.perguntasrelatosinternos = payload
  },
  ATUALIZAR_PERGUNTARELATOINTERNO (state, payload) {
    state.perguntasrelatosinternos[payload.index].pergunta = payload.pergunta
  },
  ADD_PERGUNTARELATOINTERNO (state, payload) {
    state.perguntasrelatosinternos.push(payload)
  },
  ADD_RELATOINTERNO (state, payload) {
    state.relatosInternos.push(payload)
  },
  ATUALIZAR_RELATOSINTERNOS (state, payload) {
    state.relatosInternos = payload
  },
  ATUALIZAR_RELATOINTERNO (state, payload) {
    state.relatosInternos[payload.index].dataCriacao = payload.dataCriacao
    state.relatosInternos[payload.index].horaCriacao = payload.horaCriacao
    state.relatosInternos[payload.index].horarioInicioTrabalho = payload.horarioInicioTrabalho
    state.relatosInternos[payload.index].dataAtendimentoMedico = payload.dataAtendimentoMedico
    state.relatosInternos[payload.index].horaAtendimentoMedico = payload.horaAtendimentoMedico
    state.relatosInternos[payload.index].apresentouCopiaAtestado = payload.apresentouCopiaAtestado
    state.relatosInternos[payload.index].equipe = payload.equipe
    state.relatosInternos[payload.index].placaVeiculo = payload.placaVeiculo
    state.relatosInternos[payload.index].funcionario = payload.funcionario
    state.relatosInternos[payload.index].parceiro = payload.parceiro
    state.relatosInternos[payload.index].comunicado = payload.comunicado
  }
}

export default new Vuex.Store({
  state,
  mutations
})
