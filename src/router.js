import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import DashView from './components/Dash.vue'
import LoginView from './components/Login.vue'
import NotFoundView from './components/Error404.vue'

// Import Views principais
import DashboardView from './components/dash/Dashboard.vue'

// import viws secundárias
import ListaRegionais from './components/dash/regional/lista_regional.vue'
import NovaRegional from './components/dash/regional/nova_regional.vue'
import Regional from './components/dash/regional/regional.vue'
import EditarRegional from './components/dash/regional/editar_regional.vue'

import ListaPolos from './components/dash/polo/lista_polo.vue'
import NovoPolo from './components/dash/polo/novo_polo.vue'
import Polo from './components/dash/polo/polo.vue'
import EditarPolo from './components/dash/polo/editar_polo.vue'

import ListaLocalidadesPolo from './components/dash/localidade_polo/lista_localidade_polo.vue'
import NovaLocalidadePolo from './components/dash/localidade_polo/nova_localidade_polo.vue'
import LocalidadePolo from './components/dash/localidade_polo/localidade_polo.vue'
import EditarLocalidadePolo from './components/dash/localidade_polo/editar_localidade_polo.vue'

import ListaFuncoes from './components/dash/funcao/lista_funcao.vue'
import NovaFuncao from './components/dash/funcao/nova_funcao.vue'
import EditarFuncao from './components/dash/funcao/editar_funcao.vue'
import Funcao from './components/dash/funcao/funcao.vue'

import ListaFuncionarios from './components/dash/funcionario/lista_funcionario.vue'
import NovoFuncionario from './components/dash/funcionario/novo_funcionario.vue'
import EditarFuncionario from './components/dash/funcionario/editar_funcionario.vue'
import Funcionario from './components/dash/funcionario/funcionario.vue'

import ListaTreinamentos from './components/dash/treinamento/lista_treinamento.vue'
import NovoTreinamento from './components/dash/treinamento/novo_treinamento.vue'
import EditarTreinamento from './components/dash/treinamento/editar_treinamento.vue'
import Treinamento from './components/dash/treinamento/treinamento.vue'

import ListaTreinamentosRealizados from './components/dash/treinamento_realizado/lista_treinamento_realizado.vue'
import NovoTreinamentoRealizado from './components/dash/treinamento_realizado/novo_treinamento_realizado.vue'
import EditarTreinamentoRealizado from './components/dash/treinamento_realizado/editar_treinamento_realizado.vue'
import TreinamentoRealizado from './components/dash/treinamento_realizado/treinamento_realizado.vue'

import ListaProcessos from './components/dash/processo/lista_processo.vue'
import NovoProcesso from './components/dash/processo/novo_processo.vue'
import EditarProcesso from './components/dash/processo/editar_processo.vue'
import Processo from './components/dash/processo/processo.vue'

import ListaTiposProcesso from './components/dash/tipo_processo/lista_tipo_processo.vue'
import NovoTipoProcesso from './components/dash/tipo_processo/novo_tipo_processo.vue'
import EditarTipoProcesso from './components/dash/tipo_processo/editar_tipo_processo.vue'
import TipoProcesso from './components/dash/tipo_processo/tipo_processo.vue'

import ListaEquipamentosColetivos from './components/dash/equipamento_coletivo/lista_equipamento_coletivo.vue'
import NovoEquipamentoColetivo from './components/dash/equipamento_coletivo/novo_equipamento_coletivo.vue'
import EditarEquipamentoColetivo from './components/dash/equipamento_coletivo/editar_equipamento_coletivo.vue'
import EquipamentoColetivo from './components/dash/equipamento_coletivo/equipamento_coletivo.vue'

import ListaEquipamentosIndividuais from './components/dash/equipamento_individual/lista_equipamento_individual.vue'
import NovoEquipamentoIndividual from './components/dash/equipamento_individual/novo_equipamento_individual.vue'
import EditarEquipamentoIndividual from './components/dash/equipamento_individual/editar_equipamento_individual.vue'
import EquipamentoIndividual from './components/dash/equipamento_individual/equipamento_individual.vue'

import ListaPerguntasRisco from './components/dash/pergunta_risco/lista_pergunta_risco.vue'
import NovaPerguntaRisco from './components/dash/pergunta_risco/nova_pergunta_risco.vue'
import EditarPerguntaRisco from './components/dash/pergunta_risco/editar_pergunta_risco.vue'
import PerguntaRisco from './components/dash/pergunta_risco/pergunta_risco.vue'

import ListaRiscosIdentificados from './components/dash/risco_identificado/lista_risco_identificado.vue'
import NovoRiscoIdentificado from './components/dash/risco_identificado/novo_risco_identificado.vue'
import EditarRiscoIdentificado from './components/dash/risco_identificado/editar_risco_identificado.vue'
import RiscoIdentificado from './components/dash/risco_identificado/risco_identificado.vue'

import ListaAnalisesRisco from './components/dash/form_analise_risco/lista_analise_risco.vue'
import NovaAnaliseRisco from './components/dash/form_analise_risco/nova_analise_risco.vue'
import EditarAnaliseRisco from './components/dash/form_analise_risco/editar_analise_risco.vue'
import AnaliseRisco from './components/dash/form_analise_risco/analise_risco.vue'

import ListaDesvios from './components/dash/form_desvio/lista_desvio.vue'
import NovoDesvio from './components/dash/form_desvio/novo_desvio.vue'
import EditarDesvio from './components/dash/form_desvio/editar_desvio.vue'
import Desvio from './components/dash/form_desvio/desvio.vue'

import ListaComunicadosInternos from './components/dash/form_comunicado_interno/lista_comunicado_interno.vue'
import NovoComunicadoInterno from './components/dash/form_comunicado_interno/novo_comunicado_interno.vue'
import EditarComunicadoInterno from './components/dash/form_comunicado_interno/editar_comunicado_interno.vue'
import ComunicadoInterno from './components/dash/form_comunicado_interno/comunicado_interno.vue'

import ListaUsuarios from './components/dash/usuario/lista_usuario.vue'
import NovoUsuario from './components/dash/usuario/novo_usuario.vue'
import EditarUsuario from './components/dash/usuario/editar_usuario.vue'
import Usuario from './components/dash/usuario/usuario.vue'

import ListaTarefas from './components/dash/tarefa/lista_tarefa.vue'
import NovaTarefa from './components/dash/tarefa/nova_tarefa.vue'
import EditarTarefa from './components/dash/tarefa/editar_tarefa.vue'
import Tarefa from './components/dash/tarefa/tarefa.vue'

import ListaPerguntasRelatosInternos from './components/dash/pergunta_relato/lista_pergunta_relato.vue'
import NovaPerguntaRelatoInterno from './components/dash/pergunta_relato/nova_pergunta_relato.vue'
import EditarPerguntaRelatoInterno from './components/dash/pergunta_relato/editar_pergunta_relato.vue'
import PerguntaRelatoInterno from './components/dash/pergunta_relato/pergunta_relato.vue'

import ListaRelatosInternos from './components/dash/form_relato_interno/lista_relato_interno.vue'
import NovoRelatoInterno from './components/dash/form_relato_interno/novo_relato_interno.vue'
import EditarRelatoInterno from './components/dash/form_relato_interno/editar_relato_interno.vue'
import RelatoInterno from './components/dash/form_relato_interno/relato_interno.vue'

/* function load (component) {
  return () => System.import(`components/${component}.vue`)
} */

export default new VueRouter({
  /*
   * NOTE! VueRouter "history" mode DOESN'T works for Cordova builds,
   * it is only to be used only for websites.
   *
   * If you decide to go with "history" mode, please also open /config/index.js
   * and set "build.publicPath" to something other than an empty string.
   * Example: '/' instead of current ''
   *
   * If switching back to default "hash" mode, don't forget to set the
   * build publicPath back to '' so Cordova builds work again.
   */

  routes: [
    {
      path: '/login',
      component: LoginView
    }, {
      path: '/',
      component: DashView,
      children: [
        {
          path: '',
          component: DashboardView,
          name: 'Dashboard',
          description: 'Overview of environment',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
        }, {
          path: '/regionais',
          component: ListaRegionais,
          name: 'Regionais',
          description: 'Lista das regionais',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novaregional',
              component: NovaRegional,
              name: 'NovaRegional',
              description: 'Formulario de nova regional',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editaregional',
              component: EditarRegional,
              name: 'EditarRegional',
              description: 'Formulario de edição de regional',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'veregional',
              component: Regional,
              name: 'Regional',
              description: 'Resumo de uma regional',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/polos',
          component: ListaPolos,
          name: 'Polos',
          description: 'Lista de polos',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novopolo',
              component: NovoPolo,
              name: 'NovoPolo',
              description: 'Formulario de novo polo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarpolo',
              component: EditarPolo,
              name: 'EditarPolo',
              description: 'Formulario de edição de polo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verpolo',
              component: Polo,
              name: 'Polo',
              description: 'Resumo de polo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/localidadespolo',
          component: ListaLocalidadesPolo,
          name: 'LocalidadesPolo',
          description: 'Lista de localidades de polo',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novalocalidadepolo',
              component: NovaLocalidadePolo,
              name: 'NovaLocalidadePolo',
              description: 'Formulario de localidade polo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarlocalidadepolo',
              component: EditarLocalidadePolo,
              name: 'EditarLocalidadePolo',
              description: 'Formulario de edição de localidade de polo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verlocalidadepolo',
              component: LocalidadePolo,
              name: 'LocalidadePolo',
              description: 'Resumo de localidade de polo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/funcoes',
          component: ListaFuncoes,
          name: 'Funcoes',
          description: 'Lista de funções',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novafuncao',
              component: NovaFuncao,
              name: 'NovaFuncao',
              description: 'Formulario de nova função',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarfuncao',
              component: EditarFuncao,
              name: 'EditarFuncao',
              description: 'Formulario de edição de função',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verfuncao',
              component: Funcao,
              name: 'Funcao',
              description: 'Resumo de função',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/funcionarios',
          component: ListaFuncionarios,
          name: 'Funcionarios',
          description: 'Lista de funcionarios',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novofuncionario',
              component: NovoFuncionario,
              name: 'NovoFuncionario',
              description: 'Formulario de novo funcionario',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'editarfuncionario',
              component: EditarFuncionario,
              name: 'EditarFuncionario',
              description: 'Formulario de edição de funcionario',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'verfuncionario',
              component: Funcionario,
              name: 'Funcionario',
              description: 'Resumo de funcionario',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }, {
          path: '/treinamentos',
          component: ListaTreinamentos,
          name: 'Treinamentos',
          description: 'Lista de treinamentos',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novotreinamento',
              component: NovoTreinamento,
              name: 'NovoTreinamento',
              description: 'Formulario de novo treinamento',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'editartreinamento',
              component: EditarTreinamento,
              name: 'EditarTreinamento',
              description: 'Formulario de edição de treinamento',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'vertreinamento',
              component: Treinamento,
              name: 'Treinamento',
              description: 'Resumo de treinamento',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }, {
          path: '/treinamentosrealizados',
          component: ListaTreinamentosRealizados,
          name: 'TreinamentosRealizados',
          description: 'Lista de treinamentos realizados',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novotreinamentorealizado',
              component: NovoTreinamentoRealizado,
              name: 'NovoTreinamentoRealizado',
              description: 'Formulario de novo treinamento realizado',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'editartreinamentorealizado',
              component: EditarTreinamentoRealizado,
              name: 'EditarTreinamentoRealizado',
              description: 'Formulario de edição de treinamento realizado',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'vertreinamentorealizado',
              component: TreinamentoRealizado,
              name: 'TreinamentoRealizado',
              description: 'Resumo de treinamento realizado',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }, {
          path: '/processos',
          component: ListaProcessos,
          name: 'Processos',
          description: 'Lista de processos',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novoprocesso',
              component: NovoProcesso,
              name: 'NovoProcesso',
              description: 'Formulario de Novo Processo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarprocesso',
              component: EditarProcesso,
              name: 'EditarProcesso',
              description: 'Formulario de edição de processo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verprocesso',
              component: Processo,
              name: 'Processo',
              description: 'Resumo de processo',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }, {
          path: '/tiposprocesso',
          component: ListaTiposProcesso,
          name: 'TiposProcesso',
          description: 'Lista de tipos processo',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novotipoprocesso',
              component: NovoTipoProcesso,
              name: 'NovoTipoProcesso',
              description: 'Formulario de novo tipo processo',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'editartipoprocesso',
              component: EditarTipoProcesso,
              name: 'EditarTipoProcesso',
              description: 'Formulario de edição de tipo processo',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'vertipoprocesso',
              component: TipoProcesso,
              name: 'TipoProcesso',
              description: 'Resumo de tipo processo',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }, {
          path: '/equipamentoscoletivos',
          component: ListaEquipamentosColetivos,
          name: 'EquipamentosColetivos',
          description: 'Lista de equipamentos coletivos',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novoequipamentocoletivo',
              component: NovoEquipamentoColetivo,
              name: 'NovoEquipamentoColetivo',
              description: 'Formulario de novo equipamento coletivo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarequipamentocoletivo',
              component: EditarEquipamentoColetivo,
              name: 'EditarEquipamentoColetivo',
              description: 'Formulario de edição de novo equipamento coletivo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verequipamentocoletivo',
              component: EquipamentoColetivo,
              name: 'EquipamentoColetivo',
              description: 'Resumo de equipamento coletivo',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/equipamentosindividuais',
          component: ListaEquipamentosIndividuais,
          name: 'EquipamentosIndividuais',
          description: 'Lista de equipamentos individuais',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novoequipamentoindividual',
              component: NovoEquipamentoIndividual,
              name: 'NovoEquipamentoIndividual',
              description: 'Formulario de novo equipamento individual',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarequipamentoindividual',
              component: EditarEquipamentoIndividual,
              name: 'EditarEquipamentoIndividual',
              description: 'Formulario de edição de equipamento individual',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verequipamentoindividual',
              component: EquipamentoIndividual,
              name: 'EquipamentoIndividual',
              description: 'Resumo de equipamento individual',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/perguntasrisco',
          component: ListaPerguntasRisco,
          name: 'PerguntasRisco',
          description: 'Lista de perguntas de analise preliminar de risco',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novaperguntarisco',
              component: NovaPerguntaRisco,
              name: 'NovaPerguntaRisco',
              description: 'Formulario de nova pergunta da analise preliminar de risco',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarperguntarisco',
              component: EditarPerguntaRisco,
              name: 'EditarPerguntaRisco',
              description: 'Formulario de edição de pergunta risco',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verperguntarisco',
              component: PerguntaRisco,
              name: 'PerguntaRisco',
              description: 'Resumo de pergunta risco',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/riscosidentificados',
          component: ListaRiscosIdentificados,
          name: 'RiscosIdentificados',
          description: 'Lista de riscos identificados',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novoriscoidentificado',
              component: NovoRiscoIdentificado,
              name: 'NovoRiscoIdentificado',
              description: 'Formulario de risco identificado',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editariscoidentificado',
              component: EditarRiscoIdentificado,
              name: 'EditarRiscoIdentificado',
              description: 'Formulario de risco identificado',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'veriscoidentificado',
              component: RiscoIdentificado,
              name: 'RiscoIdentificado',
              description: 'Resumo de risco identificado',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/analisesrisco',
          component: ListaAnalisesRisco,
          name: 'AnalisesRisco',
          description: 'Lista de analises preliminar de risco',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novanaliserisco',
              component: NovaAnaliseRisco,
              name: 'NovaAnaliseRisco',
              description: 'Formulario de nova analise risco',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'editaranaliserisco',
              component: EditarAnaliseRisco,
              name: 'EditarAnaliseRisco',
              description: 'Formulario de edição de analise risco',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'veranaliserisco',
              component: AnaliseRisco,
              name: 'AnaliseRisco',
              description: 'Resumo de analise risco',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }, {
          path: '/desvios',
          component: ListaDesvios,
          name: 'Desvios',
          description: 'Lista de desvios',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novodesvio',
              component: NovoDesvio,
              name: 'NovoDesvio',
              description: 'Formulario de novo desvio',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'editardesvio',
              component: EditarDesvio,
              name: 'EditarDesvio',
              description: 'Formulario de edição de desvio',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'verdesvio',
              component: Desvio,
              name: 'Desvio',
              description: 'Resumo do desvio',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }, {
          path: '/comunicadosinternos',
          component: ListaComunicadosInternos,
          name: 'ComunicadosInternos',
          description: 'Lista de comunicados internos',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novocomunicadointerno',
              component: NovoComunicadoInterno,
              name: 'NovoComunicadoInterno',
              description: 'Formulario de novo comunicado interno',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'editarcomunicadointerno',
              component: EditarComunicadoInterno,
              name: 'EditarComunicadoInterno',
              description: 'Formulários de edição de comunicado interno',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'vercomunicadointerno',
              component: ComunicadoInterno,
              name: 'ComunicadoInterno',
              description: 'Resumo do comunicado interno',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }, {
          path: '/usuarios',
          component: ListaUsuarios,
          name: 'Usuarios',
          description: 'Lista de usuarios',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novousuario',
              component: NovoUsuario,
              name: 'NovoUsuario',
              description: 'Formulario de novo usuário',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarusuario',
              component: EditarUsuario,
              name: 'EditarUsuario',
              description: 'Formulario de edição de usuário',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verusuario',
              component: Usuario,
              name: 'Usuario',
              description: 'Resumo de usuario',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/tarefas',
          component: ListaTarefas,
          name: 'Tarefas',
          description: 'Lista de tarefas',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novatarefa',
              component: NovaTarefa,
              name: 'NovaTarefa',
              description: 'Formulario de nova tarefa',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editartarefa',
              component: EditarTarefa,
              name: 'EditarTarefa',
              description: 'Formulario de edição de tarefa',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'vertarefa',
              component: Tarefa,
              name: 'Tarefa',
              description: 'Resumo de tarefa',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/perguntasrelatosinternos',
          component: ListaPerguntasRelatosInternos,
          name: 'PerguntasRelatosInternos',
          description: 'Lista de perguntas do relato interno',
          meta: { requiresAuth: true, permissao: ['admin'] },
          children: [
            {
              path: 'novaperguntarelatointerno',
              component: NovaPerguntaRelatoInterno,
              name: 'NovaPerguntaRelatoInterno',
              description: 'Formulario de perguntas de relato interno',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'editarperguntarelatointerno',
              component: EditarPerguntaRelatoInterno,
              name: 'EditarPerguntaRelatoInterno',
              description: 'Formulario de edição de pergunta de relato interno',
              meta: { requiresAuth: true, permissao: ['admin'] }
            },
            {
              path: 'verperguntarelatointerno',
              component: PerguntaRelatoInterno,
              name: 'PerguntaRelatoInterno',
              description: 'Resumo de pergunta de relato interno',
              meta: { requiresAuth: true, permissao: ['admin'] }
            }
          ]
        }, {
          path: '/relatosinternos',
          component: ListaRelatosInternos,
          name: 'RelatosInternos',
          description: 'Lista de relatos internos',
          meta: { requiresAuth: true, permissao: ['normal', 'admin'] },
          children: [
            {
              path: 'novorelatointerno',
              component: NovoRelatoInterno,
              name: 'NovoRelatoInterno',
              description: 'Formulario de novo relato interno',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'editarelatointerno',
              component: EditarRelatoInterno,
              name: 'EditarRelatoInterno',
              description: 'Formulario de edição de relato interno',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            },
            {
              path: 'verelatointerno',
              component: RelatoInterno,
              name: 'RelatoInterno',
              description: 'Resumo de relato interno',
              meta: { requiresAuth: true, permissao: ['normal', 'admin'] }
            }
          ]
        }
      ]
    }, {
    // not found handler
      path: '*',
      component: NotFoundView
    }
  ]
})
