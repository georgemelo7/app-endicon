// === DEFAULT / CUSTOM STYLE ===
// WARNING! always comment out ONE of the two require() calls below.
// 1. use next line to activate CUSTOM STYLE (./src/themes)
// require(`./themes/app.${__THEME}.styl`)
// 2. or, use next line to activate DEFAULT QUASAR STYLE
require(`quasar/dist/quasar.${__THEME}.css`)
// ==============================

import Vue from 'vue'
import Quasar from 'quasar'
import router from './router'
import store from './store'
import Resource from 'vue-resource'

Vue.use(Quasar) // Install Quasar Framework

Vue.use(Resource)

// main.js
import VueGmaps from 'vue-gmaps'
Vue.use(VueGmaps, {
  key: 'AIzaSyAnj131tKCPSetMVbCtG0k2umGcvswK048',
  libraries: ['places']
})

import * as VueGoogleMaps from 'vue2-google-maps'
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAnj131tKCPSetMVbCtG0k2umGcvswK048'
    // libraries: 'places', //// If you need to use place input
  }
})

// Import Helpers for filters
import { domain, count, prettyDate, pluralize, capitalize, reverse, simOuNao } from './filters'

function checarAutorizacao () {
  if (store.state.authenticated) {
    Vue.http.headers.common['Authorization'] = 'Bearer ' + window.localStorage.getItem('id_token')
  }
}

router.beforeEach((to, from, next) => {
  checarAutorizacao()
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!store.state.authenticated) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      if (to.meta.permissao.some(elemento => elemento === window.sessionStorage.getItem('permissao'))) {
        next()
      } else {
        store.commit('ALTERAR_ERROACESSO', {
          'erro': true
        })
        next(false)
      }
    }
  } else {
    next() // make sure to always call next()!
  }
})

// Import Install and register helper items
Vue.filter('count', count)
Vue.filter('reverse', reverse)
Vue.filter('domain', domain)
Vue.filter('prettyDate', prettyDate)
Vue.filter('pluralize', pluralize)
Vue.filter('capitalize', capitalize)
Vue.filter('simOuNao', simOuNao)

Quasar.start(() => {
  /* eslint-disable no-new */
  new Vue({
    el: '#q-app',
    router,
    store: store,
    render: h => h(require('./App'))
  })
})
